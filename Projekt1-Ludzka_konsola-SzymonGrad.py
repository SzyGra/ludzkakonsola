import re
import os
import time
import subprocess
import pyttsx3
import sys
import speech_recognition as sr

run=True
write_commands=True
clear = lambda: os.system('cls')
movies_list={'Venom':'https://www.youtube.com/watch?v=rGZOgO5gwbk&t=5498s','The Lion King':'https://www.youtube.com/watch?v=J6_YkYBKlZY','Monthy Python and The Holy Grain':'https://www.youtube.com/watch?v=9D5_V72jMtM','Heat':'https://www.youtube.com/watch?v=WP7iB71fDO4','Ronal':'https://www.youtube.com/watch?v=aX6WTAYSz-s'}
r=sr.Recognizer()

search_web_address=re.compile(r'^(page for me|page me|page|open page for me|open page me|open page)+( )([\w\+ \.\w]*)',re.IGNORECASE)
search_website=re.compile(r'^(find for me|find me|find|search for me|search me|search|look for me|look for|looking for me|looking me|looking for)+( )([\w\+ \.\w]*)',re.IGNORECASE)
show_movies_list=re.compile(r'^(show movies list for me|show me movies list|show movies list|show list|show movies|movies for me|movies|list for me|list)+( )?([\w\+ \.\w]*)',re.IGNORECASE)
play_movie=re.compile(r'^(play for me|play me|play movie for me|play movie|play)( )([\w\+ \.\w]*)(/.)?(exe)*',re.IGNORECASE)
open_program=re.compile(r'^(run for me|run me|run|open for me|go|start for me|start me|start)+( )([\w\+ \.\w]*)(/.)?(exe)*',re.IGNORECASE)
close_program=re.compile(r'^(kill for me|kill me|kill|close for me|close me|close|exit for me|exit me|exit|stop for me|stop me|stop)+( )?([\w\+ \.\w]+)(/.)?(exe)*')
search_yt=re.compile(r'^(youtube find for me|youtube find me|youtube find|youtube search for me|youtube search me|youtube search|youtube look for me|youtube look for|youtube looking for me|youtube looking me|youtube looking for|youtube)+( )([\w\+ \.\w]*)',re.IGNORECASE)

print('Witaj w programie LUDZKA KONSOLA'.center(24))
engine=pyttsx3.init();
engine.say('Ludzka Konsola. 1.0');
rate=engine.getProperty('rate');
engine.runAndWait();
engine.setProperty('rate',185);

home_drive=0

def program_commands():
        print('DOSTEPNE KOMENDY (JEZ. ANGIELSKI TYLKO!):'.center(24)+'\n')
        print('+ Uruchomienie programu o podanej nazwie (np.open,start run notepad albo run me notepad)'.center(24))
        print('- Zamknięcie programu o podanej nazwie (np. kill notepad)'.center(24))
        print('+ Otwarcie dokumentu o podanej nazwie (np. open abc.txt, konieczne rozszerzenie)'.center(24))
        print('+ Otwarcie w wyszukiwarce Google podanej frazy (np. search funny cats)'.center(24))
        print('+ Pokaż liste filmow do obejrzenia - show movies,show movies list,show films,show list'.center(24))
        print('o Otworzenie wybranego wideo na YouTube (np. youtube muzyka, youtube search muzyka itp.)'.center(24))
        print('= Otwarcie w przeglądarce podanej strony internetowej (np. open page YouTube)'.center(24))
        print('++ Odpalenie filmu z listy na YouTube (play Venom, play movie Venom)'.center(24))
        print('> Przełączenie się na komendy głosowe - speak'.center(24))
        print('- Zakonczenie dzialania programu - exit lub EXIT'.center(24)+'\n')

def search_youtube(command):
        web_rem_spaces=re.sub(r' ','+',command)
        try:
                pipeweb=subprocess.Popen('start www.youtube.com/results?search_query='+web_rem_spaces,shell=True)
                engine.say('YouTube search '+command);
                engine.runAndWait()
        except subprocess.CalledProcessError as e:
                print('error code',e.returncode,e.output)

def search_web_command(command):
        web_rem_spaces=re.sub(r' ','+',command)
        try:
                pipeweb=subprocess.Popen('start www.google.com/search?q='+web_rem_spaces,shell=True)
                engine.say('Search '+command);
                engine.runAndWait()
        except subprocess.CalledProcessError as e:
                print('error code',e.returncode,e.output)

def play_movie_web(command):
        try:
            if(command.lower()=='venom'):
                pipeweb=subprocess.Popen('start '+movies_list.get('Venom'),shell=True)
                engine.say('Movie '+command);
                engine.runAndWait()
            elif(command.lower()=='the lion king'):
                pipeweb=subprocess.Popen('start '+movies_list.get('The Lion King'),shell=True)
                engine.say('Movie '+command);
                engine.runAndWait()
            elif(command.lower()=='monthy python and the holy grain'):
                pipeweb=subprocess.Popen('start '+movies_list.get('Monthy Python and The Holy Grain'),shell=True)
                engine.say('Movie '+command);
                engine.runAndWait()
            elif(command.lower()=='heat'):
                pipeweb=subprocess.Popen('start '+movies_list.get('Heat'),shell=True)
                engine.say('Movie '+command);
                engine.runAndWait()
            elif(command.lower()=='ronal'):
                pipeweb=subprocess.Popen('start '+movies_list.get('Ronal'),shell=True)
                engine.say('Movie '+command);
                engine.runAndWait()
            else:
                print('Film doesnt exist in the base')
                engine.say('Movie '+command+' doesn exist in the base');
                engine.runAndWait()
        except subprocess.CalledProcessError as e:
                print('error code',e.returncode,e.output)

def search_web_adres(command):
        web_rem_spaces=re.sub(r' ','+',command)
        try:
                pipeweb=subprocess.Popen('start www.'+web_rem_spaces.lower(),shell=True)
                engine.say('Website '+command);
                engine.runAndWait()
        except subprocess.CalledProcessError as e:
                print('error code',e.returncode,e.output)

def open_program_command(command_open,say_command):
                try:
                        pipe=subprocess.Popen('start '+command_open,shell=True,stdout=subprocess.PIPE)
                        engine.say('Open '+say_command);
                        pipe.wait()
                        home_drive=pipe.returncode
                        engine.runAndWait() ;
                        time.sleep(2)
                        clear()
                except subprocess.CalledProcessError as e:
                        print('error code',e.returncode,e.output)
                if(home_drive==1):
                        pipe=subprocess.Popen('start '+command_open, cwd='D:\\',shell=True)
                        engine.say('Open '+say_command);
                        engine.runAndWait() ;
                        time.sleep(2)
                        clear()   

def commands_option(command):
       
       open_document=re.match(r'^(open for me|open me|open)+( )?([\w\+]*)(\.)+(txt|pdf|csv|doc|py)+',command,re.IGNORECASE)

       start_movie=play_movie.match(command)
       open_youtube=search_yt.match(command)
       open_website_google=search_website.match(command)
       open_program_check=open_program.match(command)
       kill_program=close_program.match(command)
       show_movies_list_check=show_movies_list.match(command)
       open_page_adres=search_web_address.match(command)      

       if(open_website_google):
               web_to_find=open_website_google.group(3)
               search_web_command(web_to_find)
               clear()
       elif(open_youtube):
               web_to_open=open_youtube.group(3)
               search_youtube(web_to_open)
               clear()
       elif(start_movie):
               web_to_open=start_movie.group(3)
               play_movie_web(web_to_open)
               clear()
       elif(open_document):
               document_to_open=open_document.group(3)+open_document.group(4)+open_document.group(5)
               if(open_document.group(5).lower()=='txt')or(open_document.group(5).lower()=='csv')or(open_document.group(5).lower()=='py'):
                        pipeweb=subprocess.Popen('start notepad++ '+document_to_open,cwd='C:\\Users\janek\Downloads',shell=True)
               elif(open_document.group(5).lower()=='pdf'):
                       pipeweb=subprocess.Popen('start AcroRd32.exe '+document_to_open,cwd='C:\\Users\janek\Downloads',shell=True)
               engine.say('Open document '+command);
               engine.runAndWait()
               clear()

       elif(open_page_adres):
               web_to_open=open_page_adres.group(3)
               search_web_adres(web_to_open)
               clear()
       elif(open_program_check):
               command_to_execute=open_program_check.group(3)
               calculator_open=re.match(r'^(kalkulator|calculator|something to count|counter|abacus)+',command_to_execute,re.IGNORECASE)
               if(calculator_open):
                       open_program_command('calc','calculator')
                       clear()
               else:
                       open_program_command(command_to_execute,command_to_execute)
                       clear()
       elif(show_movies_list_check):
               engine.say('Which movie do you want to see?');
               engine.runAndWait() ;
               clear()
               for film in movies_list:
                       print(film+'\n')
       elif(kill_program):
               command_to_execute_kill=kill_program.group(3)
               calculator_kill=re.match(r'^(kalkulator|calculator|something to count|counter|abacus)+',command_to_execute_kill,re.IGNORECASE)
               browser_kill=re.match(r'^(browser|chrome|google chrome|google|internet)+',command_to_execute_kill,re.IGNORECASE)
               if(calculator_kill):
                        pipe=subprocess.Popen('taskkill /f  /im calculator.exe',shell=True,stdout=subprocess.PIPE)
                        engine.say('Close calculator');
                        engine.runAndWait() ;
                        clear()
               elif(command_to_execute_kill.lower=='unity'):
                        pipe=subprocess.Popen('taskkill /f  /im Unity.exe',shell=True,stdout=subprocess.PIPE)
                        engine.say('Close unity');
                        engine.runAndWait() ;
                        clear()
               elif(browser_kill):
                        pipe=subprocess.Popen('taskkill /f  /im chrome.exe',shell=True,stdout=subprocess.PIPE)
                        engine.say('Close browser');
                        engine.runAndWait() ;
                        clear()
               else:
                        pipe=subprocess.Popen('taskkill /f  /im '+command_to_execute_kill+'.exe',shell=True,stdout=subprocess.PIPE)
                        engine.say('Close '+command_to_execute_kill);
                        engine.runAndWait() ;
                        clear()
       elif((command.lower()=='change volume') or (command.lower()=='volume')):
                 pipe=subprocess.Popen('sndvol.exe -f',shell=True,stdout=subprocess.PIPE)
                 engine.say('Change volume');
                 engine.runAndWait() ;
                 clear()
       else:
               engine.say('This is not a command');
               engine.runAndWait() ;
               print('This is not a command')
               clear()
def listen_commands():
        with sr.Microphone() as source:
                print('Speak something')
                audio=r.listen(source)
        try:
                text=r.recognize_google(audio)
                print(': {}'.format(text))
                commands_option(text)
        except:
                print('Sorry couldnt recognize your voice')
while(run):
        program_commands()
        engine.say('How can I help you?');
        engine.runAndWait() ;
        command_type=input()
        if(command_type.lower()=='exit'):
                break
        if(command_type.lower()=='speak'):
                clear()
                program_commands()
                engine.say('Start speech recognition');
                engine.runAndWait() ;
                listen_commands()
        else:
                commands_option(command_type)